import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';

import {Navigate} from 'react-router-dom';


// Step 2 Rendering | import {useNavigate} for hooking/hooks
// import {useNavigate} from "react-router-dom";

import UserContext from '../UserContext'; /*Global state setting |useContext*/

// Session - 55
import Swal from 'sweetalert2';

export default function Login() {

    const {user, setUser} = useContext(UserContext) //Global state | react HOOK

   
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")    
    const [isActive, setIsActive] = useState(false);
    // Step 2.1 Rendering |
    // const navigate = useNavigate();

    useEffect(() => {
        if((email !== "" && password !== "")) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
   
    }, [email, password]);

    
    function authenticate(e) {
        e.preventDefault()

            // Session - 55 
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // We will recieve either a token or an error response
            console.log(data);

            //Conditional statement | Store generated token in local storage
            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access)

                retrieveUserDetails(data.access);

                Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })
            } else {
                Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Please, check your login details and try again."
                });
            };
        });

        /*Step 1 Rendering| targeting localStorage */
        // set the email of the authenticated user in the local storage. 
        // SYNTAX: localStorage.setItem('propertyName', value);
        //localStorage.setItem('email', email); //SETTER

        // Sets the GLOBAL user state to have properties obtain from the localstorage.
        //setUser({email: localStorage.getItem('email')})

        setEmail("");
        setPassword("");
        // Hooking 
        // navigate("/");

        // alert("You are now logged in!")
    };

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };


    return (
        //S-55| user.email)
        (user.id) ?
        <Navigate to="/courses" />

        :
       
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                
	                type="email" 
	                placeholder="Enter email"
                    value = {email}
                    onChange = {e => setEmail(e.target.value)} 
	                required
                />
                <Form.Text className="text-muted">
                </Form.Text>
            </Form.Group>
            <br />

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
                    value = {password}
                    onChange = {e => setPassword(e.target.value)} 
	                required
                />
            </Form.Group>

            <br />

            { isActive ?
	            <Button variant="success" type="submit" id="submitBtn">
	                Login
	            </Button>
	                :
	            <Button variant="success" type="submit" id="submitBtn" disabled>
	                Login
	            </Button>
            }
            
        </Form>
    )

}