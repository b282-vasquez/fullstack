/*
	import Banner from '../components/Banner';
	import Highlights from '../components/Highlights';
	import ErrorPage from '../pages/ErrorPage';


	export default function Home () {
		return (

			<>
				<Banner />
	      		<Highlights />
			</>

			)
	}
*/

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere.",
		destination: "/courses",
		label: "Enroll now!"
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}


