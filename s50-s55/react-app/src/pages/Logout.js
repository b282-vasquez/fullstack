import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

import {Navigate} from 'react-router-dom';

export default function Logout() {

	const {unsetUser, setUser} = useContext(UserContext)

	// localStorage.clear();
	unsetUser();

	useEffect(() => {
		// S-55 | setUser({email: null})
		setUser({id: null})
	});



	return (
		<Navigate to= "/login" />
	)
}