/*import coursesData from '../data/coursesData'; s-55*/
import CourseCard from '../components/CourseCard';

import {useState, useEffect} from 'react'

export default function Courses() {
// console.log(coursesData);
	
	// 		/*Props Drilling*/
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course = {course} />
	// 	)
// })

	// s-55 | START
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			setCourses(data.map(course => {
				return (
					<CourseCard key={course.id} course = {course} />
					)
			}))
		})
	}, []);
	// s-55 | END

	return (
		<>
		{courses}
		</>
	)
};