/*import Home from '../pages/Home';
	import {Button} from 'react-bootstrap';
	import {useNavigate} from 'react-router-dom';



	export default function ErrorPage() {

		const navigate = useNavigate();
		const goHome = () => {		
			navigate("/")
		}
		return (
				<>
				<br/>
				<h1>Error 404</h1>
				<p>The page you are looking for cannot be found.</p>
				<Button variant = "primary" onClick = {goHome} >Back to Home </Button>
				</>
			)
}*/

import Banner from '../components/Banner';

export default function Error() {

    const data = { 
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Banner data={data} /> 
    )
}