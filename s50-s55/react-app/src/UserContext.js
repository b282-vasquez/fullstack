// Global state initialization | UserContext.js

// React Context - Context provides a way to share values like these between components without having to explicitly pass a prop through each component.
import React from 'react';

// Create context object and enable share data to multiple components w/o using pros manually.
// React.createContext() - is a function in the React library that creates a new CONTEXT OBJECT.
// CONTEXT OBJECT - is a data type of an object that can be used to store information and can be shared to other components within the app.
const UserContext = React.createContext(); 


//UserContext.Provider is a component that is created when you use React.createContext()
// .Provider component - allows other components to use the context object and supply the necessary information needed to the context object.
export const UserProvider = UserContext.Provider; //Use to wrap part of component | | provide information.

//UserContext - global state that can be shared and updated throughout the application.
export default UserContext;

