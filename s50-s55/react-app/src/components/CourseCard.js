//import {useState, useEffect} from 'react';

import {Row, Col, Card, Button} from 'react-bootstrap';

// ==S-55
import {Link} from 'react-router-dom';


export default function CourseCard({course}) {

    const {name, description, price, _id} = course;

//Commented Code Start
    // SYNCTAX:
    //     const [getter, setter] = useState(initialGetterValue);
    
    // const [count, setCount] = useState(0);
    // console.log(useState(0));

    // const [seats, setSeats] = useState(5);

    // function enroll(){
        
    //    // if (seats > 0) {
    //    //      setCount(count + 1);
    //    //      console.log('Enrollees: ' + count);
    //    //      setSeats(seats - 1);
    //    //      console.log('Seats: ' + seats);
    //    //  } else {
    //    //      alert("No more seats available");
    //    //  };
        
    //     // useEffect coding
    //     setCount(count + 1);
    //     setSeats(seats - 1);
    // }

    // // UseEffect is a react HOOK
    // useEffect(() => {
    //     // conditional statement
    //     if(seats <= 0) {
    //         alert('No more seats available')
    //     };
    //     // "[]" - run only one time
    //     // "[seats]" - dependency/ies - if may chance sa "seats" mag ra-run hanggang ma-reach yung condition. 
    // }, [seats]);
//Commented Code End



return (
   /* <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        <Card.Text>PhP {price}</Card.Text>
                        <Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Card.Subtitle>Enrolless: {seats}</Card.Subtitle>
                        <br/>
                        <Button variant="primary" onClick ={enroll} disabled ={seats <= 0}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>*/ 
    // ==s55
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row> 
    )
}
