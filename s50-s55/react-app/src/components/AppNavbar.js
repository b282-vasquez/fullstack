/*IMPORTING STAGE START*/
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {useState, useContext} from 'react'; //---- useState importation
import {Link, NavLink} from 'react-router-dom'; /*Step 4 Routing | import Link and NavLink*/

import UserContext from '../UserContext'; //Global setting
/*IMPORTING STAGE END*/


function AppNavbar() {

  // ---- useState
  // const [user, setUser] = useState(localStorage.getItem('email'));

  const {user} = useContext(UserContext);

  return (
    <Navbar expand="lg" className="bg-body-tertiary">

   {/*Step 5 Routing | as={Link} to = "/"*/}
        <Navbar.Brand as = {Link} to = "/" > Zuitt </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
              <Nav.Link as = {NavLink} to = "/" > Home </Nav.Link>
              <Nav.Link as = {NavLink} to = "/courses" > Courses </Nav.Link>

            {/*---If user is login, show Logout---USE ternary condition */}

            {/*S-55|(user.email)*/}
            { (user.id) ?
              <Nav.Link as = {NavLink} to = "/logout" > Logout </Nav.Link>

              :
              
              <>
                <Nav.Link as = {NavLink} to = "/login" > Login </Nav.Link>
                <Nav.Link as = {NavLink} to = "/register" > Register </Nav.Link>
              </>

            }
                      
          </Nav>
        </Navbar.Collapse>
      
    </Navbar>
  );
}

export default AppNavbar;