import {useState, useEffect} from 'react';

/*IMPORTED FROM COMPONENT FOLDER*/
import AppNavbar from './components/AppNavbar';
 //= S-55
import CourseView from './components/CourseView';

/*IMPORTED FROM PAGE FOLDER*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register'; // Step 2 Reg-page|Import it to App.js
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';



import './App.css';
import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext'; //Wrapped around components

/*Importing routing | react-router-dom*/
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'






function App() {

  //global state initial|setter start

  //const [user, setUser] = useState({email: localStorage.getItem('email')});

  // = S-55 | React.js - App Integration with fetch = | START
    const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }
 //global state initial|setter end


 // Used to check if the user information is properly stored upon login in the localStorage and cleared upon LOGOUT
 useEffect(() => {
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res => res.json())
  .then(data => {

    //user is logged in
    if(typeof data._id !== "undefined") {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })

      // user is logged out
    } else {
      setUser ({
        id: null,
        isAdmin: null
      })
    }
  })
 }, []);

  //= S-55 | React.js - App Integration with fetch = | END


  return (
    <>      
      <UserProvider value = {{user,setUser, unsetUser}} > {/*Global state setup*/}       
        <Router> {/*Step 1 Routing | wrapped around in Router*/}
              <AppNavbar />
              <Container> 
                  {/*Step 2 Routing | wrapped around in Routes*/}
                  <Routes> 
                      {/*Step 3 Routing | wrapped around in Route | <Route path = "/" />*/}
                      <Route path = "/" element = {<Home />} /> 
                      <Route path = "/courses" element = {<Courses />} /> 
                      <Route path = "/courses/:courseId" element = {<CourseView />} /> 
                      <Route path = "/register" element = {<Register />} /> 
                      <Route path = "/login" element = {<Login />} />
                      <Route path = "/logout" element = {<Logout />} />
                      <Route path = "/*" element = {<Error />} />    
                  </Routes>
              </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
