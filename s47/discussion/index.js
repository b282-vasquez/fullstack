//console.log("Hello World!");

// "document" refers to the whole page.
//"querySelector" is used to select a spesific object(HTML element) from the document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
//
const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#span-full-name");

// Alternatively, we can use the "getElement" functions to retrieve the elements.
// document.getElementById
// document.getElementByClassName
// document.getElementByTagName
// const txtFirstName = document.getElementById("txt-first-name")
// const spanFullName = document.getElementById("span-full-name")



// Whenever a user interacts with a webpage, this action is considered as an (event).
// "addEventListener" is a function that take 2 arguments
// "keyup" is a string identifying an event
// Second argument - will execute once the specified event is triggered
txtFirstName.addEventListener("keyup", (event) => {
	// "innerHTML" property that sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;

	//".target" contains the element where the event happed.
	console.log(event.target);
	// ".value" gets the value of the input object
	console.log(event.target.value);
})




